(defpackage #:crash.reload
  (:use #:cl #:crash))

(in-package #:crash.reload)

(asdf:defsystem #:crash.reload
  :description "Reload plugin for crash"
  :author "Florian Margaine <florian@margaine.com>"
  :license "MIT License"
  :serial t
  :depends-on (:crash)
  :components ((:file "reload")))
