(in-package #:crash)

(export '*default-contrib-modules*)
(defvar *default-contrib-modules* '("cd" "history" "expansion" "alias"))

(defvar *default-contrib-folders* `(#p "/usr/share/crash/contrib/"
                                       ,(merge-pathnames #p ".crash/contrib/"
                                                         (user-homedir-pathname))))

(export 'main)
(defun main ()
  (let ((user-file (merge-pathnames #p ".crashrc" (user-homedir-pathname))))
    (when (probe-file user-file)
      (load user-file)))
  (map nil #'setup-modules-load-path *default-contrib-folders*)
  (map nil #'load-module *default-contrib-modules*)
  (handler-case
      (call-startup-hooks (ordered-hooks *hooks* :startup))
    (error (e) (format t "Startup hook error: ~A~%" e)))
  (loop
     (handler-case
         (let* ((line (rl:readline :prompt (funcall *prompt-callback*)
                                   :add-history t))
                (parts (parse-parts line))
                (command (first parts))
                (arguments (rest parts)))
           (handler-case
               (call-command-hooks (ordered-hooks *hooks* :command) command arguments)
             (error (e) (format t "Command hook error: ~A~%" e)))
           (force-output))
       (sb-sys:interactive-interrupt ()
         (loop for child = (pop *children*)
            until (null child)
            do (sb-posix:kill child sb-unix:sigint))))))

(defun parse-parts (line)
  "Parse the parts of a command line.

Generally speaking, (read-from-string) with the correct
readtable is good enough, given that we (symbol-name)
every component. Some special cases exist though, like ..
or |, so we need to special-case them. The current way
is super ugly, but tweaking the reader to handle .. properly
is harder than I originally thought. Let's revisit this later."
  (handler-case
      (let ((*readtable* (copy-readtable nil)))
        (setf (readtable-case *readtable*) :preserve)
        ;; Take #\! as a constituent syntax type
        (set-syntax-from-char #\| #\!)
        (mapcar
         (lambda (part) (if (symbolp part) (symbol-name part) part))
         (read-from-string
          (format
           nil "(~A)"
           (ppcre:regex-replace-all "\\.\\." line "\"..\"")))))
    (error (e) (format t "Unable to parse line: ~A~%" e))))

(defun call-startup-hooks (hooks)
  (when hooks
    (funcall (first hooks)
             (lambda ()
               (when (rest hooks)
                 (call-startup-hooks (rest hooks)))))))

(defun call-command-hooks (hooks command arguments)
  (when hooks
    (funcall (first hooks)
             command
             arguments
             (lambda (command arguments)
               (when (rest hooks)
                 (call-command-hooks (rest hooks) command arguments))))))
