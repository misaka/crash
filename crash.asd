(defpackage #:crash
  (:use #:cl))

(defpackage #:crash-user
  (:use #:cl #:crash))

(in-package #:crash)

(asdf:defsystem #:crash
  :description "Yet Another Shell"
  :author "Florian Margaine <florian@margaine.com>"
  :license "MIT License"
  :serial t
  :depends-on (:cl-readline :sb-posix :cffi :cl-ppcre)
  :build-operation asdf:program-op
  :build-pathname "bin/crash"
  :entry-point "crash:main"
  :components ((:module "src"
                :components ((:file "crash" :depends-on ("prompt" "defaults" "module"))
                             (:file "prompt")
                             (:file "hooks")
                             (:file "defaults" :depends-on ("hooks"))
                             (:file "module")))))
